package Interface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Canvas;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import com.google.gson.Gson;

import Datos.Equipo;
import Datos.InfoJson;
import Datos.Jugador;
import Negocio.LogicaEquipo;

import javax.swing.JPanel;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class Principal {
	private JFrame frame;
	private JTextField txtSiNoDispones;
	private JTextField textNombre;
	private JTextField textCalidad;
	private JTextField textPuesto;
	private JTextField textNoJuegaCon;
	private JTextField txtSiYaDispones;
	private JTextField txtListaDeCandidatos;
	private JTextField txtResultados;
	private JTextField delantero1;
	private JTextField delantero2;
	private JTextField delantero3;
	private JTextField medio1;
	private JLabel lblMedio;
	private JTextField medio2;
	private JTextField medio3;
	private JLabel lblDefensores;
	private JTextField defensa1;
	private JTextField defensa2;
	private JTextField defensa3;
	private JTextField defensa4;
	private JLabel lblArquero;
	private JTextField arquero;
	private JSeparator separator;
	private JSeparator separator_1;
	private JSeparator separator_2;
	
	private ArrayList<Jugador> jugadores;
	private JTextField jtextCalidadTotal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		JFileChooser buscador= new JFileChooser();
		buscador.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		Gson gson = new Gson();
		jugadores=new ArrayList<Jugador>();
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 888, 572);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtSiNoDispones = new JTextField();
		txtSiNoDispones.setBounds(10, 11, 440, 20);
		txtSiNoDispones.setFont(new Font("Arial", Font.PLAIN, 10));
		txtSiNoDispones.setEditable(false);
		txtSiNoDispones.setText("Si no dispones de una lista de candidatos en formato json aqui puedes crearla paso a paso");
		frame.getContentPane().add(txtSiNoDispones);
		txtSiNoDispones.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 42, 54, 14);
		frame.getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setBounds(87, 42, 139, 20);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Calidad:");
		lblNewLabel.setBounds(299, 42, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		textCalidad = new JTextField();
		textCalidad.setBounds(355, 42, 46, 20);
		textCalidad.setToolTipText("Numero entre 1 y 10");
		frame.getContentPane().add(textCalidad);
		textCalidad.setColumns(10);
		
		JLabel lblPosicin = new JLabel("Puesto:");
		lblPosicin.setBounds(299, 85, 54, 14);
		frame.getContentPane().add(lblPosicin);
		
		textPuesto = new JTextField();
		textPuesto.setBounds(355, 82, 86, 20);
		textPuesto.setToolTipText("Texto permitido: arquero; defensor; medio; delantero.\r\n");
		frame.getContentPane().add(textPuesto);
		textPuesto.setColumns(10);
		
		JLabel lblNoJuegaCon = new JLabel("No juega con:");
		lblNoJuegaCon.setBounds(10, 85, 86, 14);
		frame.getContentPane().add(lblNoJuegaCon);
		
		textNoJuegaCon = new JTextField();
		textNoJuegaCon.setBounds(87, 82, 139, 20);
		frame.getContentPane().add(textNoJuegaCon);
		textNoJuegaCon.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 204, 278, 302);
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frame.getContentPane().add(scrollPane);
		
		JTextArea listaCandidatos = new JTextArea();
		listaCandidatos.setEditable(false);
		scrollPane.setViewportView(listaCandidatos);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(110, 113, 89, 23);
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(controlarCalidad(textCalidad.getText()) && controlarPuesto(textPuesto.getText())) {
					jugadores.add(new Jugador(textNombre.getText(),textCalidad.getText(),textPuesto.getText(),textNoJuegaCon.getText()));
					listaCandidatos.setText(listaCandidatos.getText()+textNombre.getText()+" "+textCalidad.getText()+" "+textPuesto.getText()+" "+textNoJuegaCon.getText()+"\n");
				}else {
					infoBox("Alguna casilla est� vac�a o es incorrecta", "No se pudo agregar");
				}
			}
		});
		frame.getContentPane().add(btnAgregar);
		
		txtSiYaDispones = new JTextField();
		txtSiYaDispones.setBounds(593, 11, 204, 20);
		txtSiYaDispones.setEditable(false);
		txtSiYaDispones.setFont(new Font("Arial", Font.PLAIN, 10));
		txtSiYaDispones.setText("Si ya dispones de Json entonces cargalo");
		frame.getContentPane().add(txtSiYaDispones);
		txtSiYaDispones.setColumns(10);
		
		JButton btnCrearEquipo = new JButton("Crear equipo");
		btnCrearEquipo.setEnabled(false);
		btnCrearEquipo.setBounds(312, 310, 110, 45);
		btnCrearEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InfoJson candidatos=new InfoJson(buscador.getSelectedFile().getAbsolutePath());
				ArrayList<ArrayList<Jugador>> grupos=LogicaEquipo.separarEnGrupos(candidatos.getInformacion());
				ArrayList<ArrayList<Jugador>> subGrupoArqueros= LogicaEquipo.armarSubGruposDe1(grupos);
				ArrayList<ArrayList<Jugador>> subGrupoDefensores= LogicaEquipo.armarSubGruposDe4(grupos,1);
				ArrayList<ArrayList<Jugador>> subGrupoMedio= LogicaEquipo.armarSubGruposDe3(grupos,2);
				ArrayList<ArrayList<Jugador>> subGrupoDelanteros= LogicaEquipo.armarSubGruposDe3(grupos,3);
				
				ArrayList<ArrayList<ArrayList<Jugador>>> gruposUnidos= LogicaEquipo.unirSubGrupos(subGrupoArqueros, subGrupoDefensores, subGrupoMedio, subGrupoDelanteros);
				ArrayList<Equipo> equiposPermutados=LogicaEquipo.permutarGrupos(gruposUnidos);
				if(equiposPermutados.size()!=0) {
					Equipo elMejor= LogicaEquipo.devolverMejor(equiposPermutados);	
				
					arquero.setText(elMejor.getArquero().get(0).getNombre());
				
					delantero1.setText(elMejor.getDelanteros().get(0).getNombre());
					delantero2.setText(elMejor.getDelanteros().get(1).getNombre());
					delantero3.setText(elMejor.getDelanteros().get(2).getNombre());
				
					medio1.setText(elMejor.getMediocampistas().get(0).getNombre());
					medio2.setText(elMejor.getMediocampistas().get(1).getNombre());
					medio3.setText(elMejor.getMediocampistas().get(2).getNombre());
				
					defensa1.setText(elMejor.getDefensores().get(0).getNombre());
					defensa2.setText(elMejor.getDefensores().get(1).getNombre());
					defensa3.setText(elMejor.getDefensores().get(2).getNombre());
					defensa4.setText(elMejor.getDefensores().get(3).getNombre());
					
					jtextCalidadTotal.setText(elMejor.getCalidadTotal().toString());
				}else {
					infoBox("No existe un equipo posible","error");
				}
			}
		});
		frame.getContentPane().add(btnCrearEquipo);
		
		JButton btnCargar = new JButton("Cargar");
		btnCargar.setBounds(644, 42, 119, 57);
		btnCargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int seleccion = buscador.showOpenDialog(buscador);
				if(seleccion == buscador.APPROVE_OPTION) {
				InfoJson candidatos=new InfoJson(buscador.getSelectedFile().getAbsolutePath());
				if(candidatos.getInformacion().size()==0)
					infoBox("No existe el archivo json o est� vacio", "error");
				else {
					try {
						listaCandidatos.setText(InfoJson.toString(candidatos.getInformacion()));
						btnAgregar.setEnabled(false);
						btnCrearEquipo.setEnabled(true);
						}catch(Exception e) {
							infoBox("Archivo incompatible", "error");
						}
					}
				}
			}
		});
		frame.getContentPane().add(btnCargar);
		
		
		
		txtListaDeCandidatos = new JTextField();
		txtListaDeCandidatos.setBounds(25, 160, 110, 20);
		txtListaDeCandidatos.setFont(new Font("Arial", Font.PLAIN, 11));
		txtListaDeCandidatos.setEditable(false);
		txtListaDeCandidatos.setText("Lista de candidatos:");
		frame.getContentPane().add(txtListaDeCandidatos);
		txtListaDeCandidatos.setColumns(10);
		
		txtResultados = new JTextField();
		txtResultados.setBounds(443, 173, 78, 20);
		txtResultados.setFont(new Font("Arial", Font.PLAIN, 11));
		txtResultados.setEditable(false);
		txtResultados.setText("Calidad total:");
		frame.getContentPane().add(txtResultados);
		txtResultados.setColumns(10);
		
		JLabel lblDelanteros = new JLabel("Delanteros:");
		lblDelanteros.setBounds(618, 214, 86, 14);
		frame.getContentPane().add(lblDelanteros);
		
		delantero1 = new JTextField();
		delantero1.setBounds(443, 239, 128, 20);
		delantero1.setEditable(false);
		frame.getContentPane().add(delantero1);
		delantero1.setColumns(10);
		
		delantero2 = new JTextField();
		delantero2.setBounds(593, 239, 128, 20);
		delantero2.setEditable(false);
		frame.getContentPane().add(delantero2);
		delantero2.setColumns(10);
		
		delantero3 = new JTextField();
		delantero3.setBounds(734, 239, 128, 20);
		delantero3.setEditable(false);
		frame.getContentPane().add(delantero3);
		delantero3.setColumns(10);
		
		medio1 = new JTextField();
		medio1.setBounds(443, 308, 128, 20);
		medio1.setEditable(false);
		frame.getContentPane().add(medio1);
		medio1.setColumns(10);
		
		lblMedio = new JLabel("Medio:");
		lblMedio.setBounds(618, 284, 46, 14);
		frame.getContentPane().add(lblMedio);
		
		medio2 = new JTextField();
		medio2.setBounds(593, 308, 128, 20);
		medio2.setEditable(false);
		frame.getContentPane().add(medio2);
		medio2.setColumns(10);
		
		medio3 = new JTextField();
		medio3.setBounds(734, 308, 128, 20);
		medio3.setEditable(false);
		frame.getContentPane().add(medio3);
		medio3.setColumns(10);
		
		lblDefensores = new JLabel("Defensores:");
		lblDefensores.setBounds(618, 356, 75, 14);
		frame.getContentPane().add(lblDefensores);
		
		defensa1 = new JTextField();
		defensa1.setBounds(508, 381, 128, 20);
		defensa1.setEditable(false);
		frame.getContentPane().add(defensa1);
		defensa1.setColumns(10);
		
		defensa2 = new JTextField();
		defensa2.setBounds(654, 381, 128, 20);
		defensa2.setEditable(false);
		frame.getContentPane().add(defensa2);
		defensa2.setColumns(10);
		
		defensa3 = new JTextField();
		defensa3.setBounds(443, 412, 128, 20);
		defensa3.setEditable(false);
		frame.getContentPane().add(defensa3);
		defensa3.setColumns(10);
		
		defensa4 = new JTextField();
		defensa4.setBounds(734, 412, 128, 20);
		defensa4.setEditable(false);
		frame.getContentPane().add(defensa4);
		defensa4.setColumns(10);
		
		lblArquero = new JLabel("Arquero:");
		lblArquero.setBounds(618, 446, 67, 14);
		frame.getContentPane().add(lblArquero);
		
		arquero = new JTextField();
		arquero.setBounds(593, 471, 128, 20);
		arquero.setEditable(false);
		frame.getContentPane().add(arquero);
		arquero.setColumns(10);
		
		separator = new JSeparator();
		separator.setBounds(0, 147, 872, 1);
		separator.setForeground(Color.BLACK);
		separator.setBackground(Color.BLACK);
		frame.getContentPane().add(separator);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(520, 0, 1, 148);
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setForeground(Color.BLACK);
		separator_1.setBackground(Color.BLACK);
		frame.getContentPane().add(separator_1);
		
		separator_2 = new JSeparator();
		separator_2.setBounds(428, 147, 1, 386);
		separator_2.setForeground(Color.BLACK);
		separator_2.setBackground(Color.BLACK);
		separator_2.setOrientation(SwingConstants.VERTICAL);
		frame.getContentPane().add(separator_2);
		
		JButton btnAJson = new JButton("A Json");
		btnAJson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String json = gson.toJson(jugadores);
				int seleccion = buscador.showSaveDialog(buscador);
				if(seleccion == buscador.APPROVE_OPTION) {
				String archivoJson=buscador.getSelectedFile().getAbsolutePath();
				try {
					FileWriter file = new FileWriter(archivoJson);
					file.write(json);
					file.close();
					if(jugadores.size()>0)
						infoBox("Json creado, ahora cargalo", "Exito");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		});
		btnAJson.setBounds(336, 113, 86, 23);
		frame.getContentPane().add(btnAJson);
		
		JTextField lblNombrecalidadposicionincompatible = new JTextField("Nombre---Calidad---Posicion---Incompatible");
		lblNombrecalidadposicionincompatible.setEditable(false);
		lblNombrecalidadposicionincompatible.setBounds(24, 190, 278, 14);
		frame.getContentPane().add(lblNombrecalidadposicionincompatible);
		
		jtextCalidadTotal = new JTextField();
		jtextCalidadTotal.setEditable(false);
		jtextCalidadTotal.setBounds(519, 173, 36, 20);
		frame.getContentPane().add(jtextCalidadTotal);
		jtextCalidadTotal.setColumns(10);
	}
	
	private static boolean controlarCalidad(String calidad) {
		if(calidad.equals(""))
			return false;
		if(Integer.parseInt(calidad)>10 || Integer.parseInt(calidad)<0)
			return false;
		return true;
	}
	
	private static boolean controlarPuesto(String puesto) {
		if(puesto.equals("arquero") || puesto.equals("defensor") || puesto.equals("medio") || puesto.equals("delantero"))
			return true;
		return false;
	}
	
	 public static void infoBox(String infoMessage, String titleBar)
	    {
	        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
	    }
}
