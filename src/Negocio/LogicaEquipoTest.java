package Negocio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import Datos.Equipo;
import Datos.Jugador;

class LogicaEquipoTest {
	
	
	@Test
	void testSepararEnGrupos() {
		ArrayList <Jugador> jugadores=new ArrayList <Jugador>();
		jugadores.add(new Jugador("jugadorx", "9", "defensor", "jugador2"));
		jugadores.add(new Jugador("jugador1", "9", "arquero", "jugador2"));
		jugadores.add(new Jugador("jugador2", "6", "arquero", "jugador3"));
		jugadores.add(new Jugador("jugador3", "7", "arquero", "jugador4"));
		jugadores.add(new Jugador("jugador4", "10", "defensor", "jugador5"));
		jugadores.add(new Jugador("jugador5", "2", "delantero", "jugador6"));
		jugadores.add(new Jugador("jugador6", "6", "defensor", "jugador7"));
		jugadores.add(new Jugador("jugador7", "9", "defensor", "jugador8"));
		jugadores.add(new Jugador("jugador8", "6", "defensor", "jugador9"));
		jugadores.add(new Jugador("jugador9", "7", "defensor", "jugador10"));
		jugadores.add(new Jugador("jugador10", "2", "medio", "jugador11"));
		jugadores.add(new Jugador("jugador11", "5", "medio", "jugador12"));
		jugadores.add(new Jugador("jugador12", "7", "medio", "jugador13"));
		jugadores.add(new Jugador("jugador13", "4", "medio", "jugador14"));
		jugadores.add(new Jugador("jugador14", "6", "delantero", "jugador15"));
		jugadores.add(new Jugador("jugador15", "3", "delantero", "jugador16"));
		jugadores.add(new Jugador("jugador16", "9", "delantero", "jugador17"));
		jugadores.add(new Jugador("jugador17", "6", "delantero", "jugador18"));
		jugadores.add(new Jugador("jugador18", "6", "delantero", "jugador1"));
		
		ArrayList<ArrayList<Jugador>> ret=LogicaEquipo.separarEnGrupos(jugadores);
		
		assertEquals(ret.get(0).size(),3);
		assertEquals(ret.get(1).size(),6);
		assertEquals(ret.get(2).size(),4);
		assertEquals(ret.get(3).size(),6);
			
	}

	@Test
	void testArmarSubGruposDe1() {
		ArrayList <ArrayList<Jugador>> jugadores= new ArrayList<ArrayList<Jugador>>();
		ArrayList <Jugador> subGrupo=new ArrayList<Jugador>();
		Jugador jugador1=new Jugador("jugador1", "9", "", "");
		Jugador jugador2=new Jugador("jugador2", "6", "", "");
		Jugador jugador3=new Jugador("jugador3", "7", "", "");
		subGrupo.add(jugador1);
		subGrupo.add(jugador2);
		subGrupo.add(jugador3);
		jugadores.add(subGrupo);
		ArrayList <ArrayList<Jugador>> armado=LogicaEquipo.armarSubGruposDe1(jugadores);
		assertIguales(armado.get(0).get(0),jugador1);
		assertIguales(armado.get(1).get(0),jugador2);
		assertIguales(armado.get(2).get(0),jugador3);
		assertEquals(armado.size(),3);
		
		
	}
	@Test
	void testArmarSubGruposDe4() {
		ArrayList <ArrayList<Jugador>> jugadores= new ArrayList<ArrayList<Jugador>>();
		ArrayList <Jugador> subGrupo=new ArrayList<Jugador>();
		Jugador jugador1=new Jugador("jugador1", "9", "", "");
		Jugador jugador2=new Jugador("jugador2", "6", "", "");
		Jugador jugador3=new Jugador("jugador3", "7", "", "");
		Jugador jugador4=new Jugador("jugador4", "10", "", "");
		Jugador jugador5=new Jugador("jugador5", "4", "", "");
		subGrupo.add(jugador1);
		subGrupo.add(jugador2);
		subGrupo.add(jugador3);
		subGrupo.add(jugador4);
		subGrupo.add(jugador5);
		jugadores.add(subGrupo);
		jugadores.add(subGrupo);
		ArrayList <ArrayList<Jugador>> armado=LogicaEquipo.armarSubGruposDe4(jugadores,1);
		assertEquals(armado.size(),factorialRec(jugadores.get(1).size())/factorialRec(jugadores.get(1).size()-4)); 
	}
	
	@Test
	void testArmarSubGruposDe3() {
		ArrayList <ArrayList<Jugador>> jugadores= new ArrayList<ArrayList<Jugador>>();
		ArrayList <Jugador> subGrupo=new ArrayList<Jugador>();
		Jugador jugador1=new Jugador("jugador1", "9", "", "");
		Jugador jugador2=new Jugador("jugador2", "6", "", "");
		Jugador jugador3=new Jugador("jugador3", "7", "", "");
		Jugador jugador4=new Jugador("jugador4", "10", "", "");
		Jugador jugador5=new Jugador("jugador5", "4", "", "");
		subGrupo.add(jugador1);
		subGrupo.add(jugador2);
		subGrupo.add(jugador3);
		subGrupo.add(jugador4);
		subGrupo.add(jugador5);
		jugadores.add(subGrupo);
		jugadores.add(subGrupo);
		jugadores.add(subGrupo);
		ArrayList <ArrayList<Jugador>> armado=LogicaEquipo.armarSubGruposDe3(jugadores,2);
		assertEquals(armado.size(),factorialRec(jugadores.get(2).size())/factorialRec(jugadores.get(2).size()-3)); 
		
	}
	@Test
	void testControlarCompatibilidad() {
		ArrayList<Jugador> arquero=new ArrayList<Jugador>();
		ArrayList<Jugador> defensores=new ArrayList<Jugador>();
		ArrayList<Jugador> mediocampistas=new ArrayList<Jugador>();
		ArrayList<Jugador> delanteros=new ArrayList<Jugador>();
		
		arquero.add(new Jugador("jugador1", "9", "arquero", "jugador2"));
		defensores.add(new Jugador("jugador6", "6", "defensor", "jugador7x"));
		defensores.add(new Jugador("jugador7", "9", "defensor", "jugador8x"));
		defensores.add(new Jugador("jugador8", "6", "defensor", "jugador9x"));
		defensores.add(new Jugador("jugador9", "7", "defensor", "jugador10x"));
		mediocampistas.add(new Jugador("jugador10", "2", "medio", "jugador11x"));
		mediocampistas.add(new Jugador("jugador11", "5", "medio", "jugador12x"));
		mediocampistas.add(new Jugador("jugador12", "7", "medio", "jugador13x"));
		delanteros.add(new Jugador("jugador16", "9", "delantero", "jugador17x"));
		delanteros.add(new Jugador("jugador17", "6", "delantero", "jugador18x"));
		delanteros.add(new Jugador("jugador18", "6", "delantero", "jugador1"));
	
		Equipo jugadores=new Equipo(arquero,defensores,mediocampistas,delanteros);
		
		assertFalse(LogicaEquipo.controlarCompatibilidad(jugadores));

	}
	
	@Test
	void testPermutarGrupos() {
		ArrayList <Jugador> jugadores=new ArrayList <Jugador>();
		jugadores.add(new Jugador("a", "2", "arquero", ""));
		jugadores.add(new Jugador("b", "10", "arquero", "m"));
		jugadores.add(new Jugador("c", "6", "defensor", ""));
		jugadores.add(new Jugador("d", "9", "defensor", ""));
		jugadores.add(new Jugador("e", "6", "defensor", ""));
		jugadores.add(new Jugador("f", "7", "defensor", ""));
		jugadores.add(new Jugador("g", "2", "defensor", ""));
		jugadores.add(new Jugador("h", "5", "defensor", ""));
		jugadores.add(new Jugador("i", "4", "medio", ""));
		jugadores.add(new Jugador("j", "6", "medio", ""));
		jugadores.add(new Jugador("k", "3", "medio", ""));
		jugadores.add(new Jugador("l", "9", "medio", "d"));
		jugadores.add(new Jugador("m", "10", "medio", ""));
		jugadores.add(new Jugador("n", "2", "delantero", ""));
		jugadores.add(new Jugador("o", "10", "delantero", "q"));
		jugadores.add(new Jugador("p", "6", "delantero", ""));
		jugadores.add(new Jugador("q", "9", "delantero", ""));
		jugadores.add(new Jugador("r", "6", "delantero", ""));

		ArrayList<ArrayList<Jugador>> ret=LogicaEquipo.separarEnGrupos(jugadores);
		ArrayList<ArrayList<Jugador>> arqueros=LogicaEquipo.armarSubGruposDe1(ret);
		ArrayList<ArrayList<Jugador>> defensores=LogicaEquipo.armarSubGruposDe4(ret,1);
		ArrayList<ArrayList<Jugador>> medios=LogicaEquipo.armarSubGruposDe3(ret,2);
		ArrayList<ArrayList<Jugador>> delanteros=LogicaEquipo.armarSubGruposDe3(ret,3);
		ArrayList<ArrayList<ArrayList<Jugador>>> permutaciones=LogicaEquipo.unirSubGrupos(arqueros, defensores, medios, delanteros);
		
		ArrayList<Equipo> permutados=LogicaEquipo.permutarGrupos(permutaciones);
		
	}
	
	@Test
	void testDevolverMejor() {
		ArrayList<Jugador> arquero = new ArrayList<Jugador>();
		ArrayList<Jugador> defensores = new ArrayList<Jugador>();
		ArrayList<Jugador> mediocampistas = new ArrayList<Jugador>();
		ArrayList<Jugador> delanteros = new ArrayList<Jugador>();
		
		ArrayList<Jugador> arquero2 = new ArrayList<Jugador>();
		ArrayList<Jugador> defensores2 = new ArrayList<Jugador>();
		ArrayList<Jugador> mediocampistas2 = new ArrayList<Jugador>();
		ArrayList<Jugador> delanteros2 = new ArrayList<Jugador>();
		
		ArrayList<Jugador> arquero3 = new ArrayList<Jugador>();
		ArrayList<Jugador> defensores3 = new ArrayList<Jugador>();
		ArrayList<Jugador> mediocampistas3 = new ArrayList<Jugador>();
		ArrayList<Jugador> delanteros3 = new ArrayList<Jugador>();
		
		//Medio
		arquero.add(new Jugador("jugador2!", "6", "arquero", "jugador3x"));
		defensores.add(new Jugador("jugador4", "10", "defensor", "jugador6"));
		defensores.add(new Jugador("jugador6", "6", "defensor", "jugador7x"));
		defensores.add(new Jugador("jugador7", "9", "defensor", "jugador8x"));
		defensores.add(new Jugador("jugador8", "6", "defensor", "jugador9x"));
		mediocampistas.add(new Jugador("jugador10", "2", "medio", "jugador11x"));
		mediocampistas.add(new Jugador("jugador11", "5", "medio", "jugador12x"));
		mediocampistas.add(new Jugador("jugador13", "4", "medio", "jugador14x"));
		delanteros.add(new Jugador("jugador14", "6", "delantero", "jugador15x"));
		delanteros.add(new Jugador("jugador15", "3", "delantero", "jugador16x"));
		delanteros.add(new Jugador("jugador16", "9", "delantero", "jugador17x"));
		//Mejor
		arquero2.add(new Jugador("jugador2!!", "7", "arquero", "jugador3x"));
		defensores2.add(new Jugador("jugador4", "10", "defensor", "jugador6"));
		defensores2.add(new Jugador("jugador6", "6", "defensor", "jugador7x"));
		defensores2.add(new Jugador("jugador7", "9", "defensor", "jugador8x"));
		defensores2.add(new Jugador("jugador8", "6", "defensor", "jugador9x"));
		mediocampistas2.add(new Jugador("jugador10", "3", "medio", "jugador11x"));
		mediocampistas2.add(new Jugador("jugador11", "5", "medio", "jugador12x"));
		mediocampistas2.add(new Jugador("jugador13", "4", "medio", "jugador14x"));
		delanteros2.add(new Jugador("jugador14", "7", "delantero", "jugador15x"));
		delanteros2.add(new Jugador("jugador15", "3", "delantero", "jugador16x"));
		delanteros2.add(new Jugador("jugador16", "9", "delantero", "jugador17x"));
		//Peor
		arquero3.add(new Jugador("jugador2!!!", "1", "arquero", "jugador3x"));
		defensores3.add(new Jugador("jugador4", "10", "defensor", "jugador6"));
		defensores3.add(new Jugador("jugador6", "6", "defensor", "jugador7x"));
		defensores3.add(new Jugador("jugador7", "9", "defensor", "jugador8x"));
		defensores3.add(new Jugador("jugador8", "6", "defensor", "jugador9x"));
		mediocampistas3.add(new Jugador("jugador10", "2", "medio", "jugador11x"));
		mediocampistas3.add(new Jugador("jugador11", "5", "medio", "jugador12x"));
		mediocampistas3.add(new Jugador("jugador13", "4", "medio", "jugador14x"));
		delanteros3.add(new Jugador("jugador14", "6", "delantero", "jugador15x"));
		delanteros3.add(new Jugador("jugador15", "3", "delantero", "jugador16x"));
		delanteros3.add(new Jugador("jugador16", "9", "delantero", "jugador17x"));
		
		ArrayList<Equipo> candidatos=new ArrayList<Equipo>();
		
		Equipo equipo1=new Equipo(arquero,defensores,mediocampistas,delanteros);
		Equipo equipo2=new Equipo(arquero2,defensores2,mediocampistas2,delanteros2);
		Equipo equipo3=new Equipo(arquero3,defensores3,mediocampistas3,delanteros3);
		
		candidatos.add(equipo1);
		candidatos.add(equipo2);
		candidatos.add(equipo3);
		
		assertEquals(equipo2,LogicaEquipo.devolverMejor(candidatos));
		
	}
	
	private boolean assertIguales(Jugador primero,Jugador otro) {
		if(primero.getNombre().equals(otro.getNombre()) && primero.getCalidad().equals(otro.getCalidad()) && primero.getPuesto().equals(otro.getPuesto()) && primero.getNoJuegaCon().equals(otro.getNoJuegaCon()))
			return true;
		return false;
	}
	
	
	static int factorialRec(int n){
		if (n==1){
			return 1;
		}else{
			return n*factorialRec(n-1);
		}
	}

}
