package Negocio;

import java.util.ArrayList;
import java.util.Iterator;

import Datos.Equipo;
import Datos.Jugador;

public class LogicaEquipo {

	public static ArrayList<ArrayList<Jugador>> separarEnGrupos(ArrayList<Jugador> candidatos) {

		ArrayList<Jugador> arqueros = new ArrayList<Jugador>();
		ArrayList<Jugador> defensores = new ArrayList<Jugador>();
		ArrayList<Jugador> medios = new ArrayList<Jugador>();
		ArrayList<Jugador> delanteros = new ArrayList<Jugador>();
		
		
		for (int i = 0; i < candidatos.size(); i++) {
			if (candidatos.get(i).getPuesto().equals("arquero")) 
				arqueros.add(candidatos.get(i));
			if (candidatos.get(i).getPuesto().equals("defensor"))
				defensores.add(candidatos.get(i));
			if (candidatos.get(i).getPuesto().equals("medio"))
				medios.add(candidatos.get(i));
			if (candidatos.get(i).getPuesto().equals("delantero"))
				delanteros.add(candidatos.get(i));
		}

		ArrayList<ArrayList<Jugador>> grupos = new ArrayList<ArrayList<Jugador>>();
		grupos.add(arqueros);
		grupos.add(defensores);
		grupos.add(medios);
		grupos.add(delanteros);

		return grupos;

	}

	public static ArrayList<ArrayList<Jugador>> armarSubGruposDe1(ArrayList<ArrayList<Jugador>> grupos) {
		ArrayList<ArrayList<Jugador>> ret = new ArrayList<ArrayList<Jugador>>();
		for (int j = 0; j < grupos.get(0).size(); j++) {
			ArrayList<Jugador> subGrupo = new ArrayList<Jugador>();
			subGrupo.add(grupos.get(0).get(j));
			ret.add(subGrupo);
			
		}

		return ret;

	}

	public static ArrayList<ArrayList<Jugador>> armarSubGruposDe4(ArrayList<ArrayList<Jugador>> grupos, int n) {
		ArrayList<ArrayList<Jugador>> ret = new ArrayList<ArrayList<Jugador>>();
		for (int i = 0; i < grupos.get(n).size(); i++) {
			for (int j = 0; j < grupos.get(n).size(); j++) {
				for (int k = 0; k < grupos.get(n).size(); k++) {
					for (int l = 0; l < grupos.get(n).size(); l++) {
						if(i!=j && i!=k && i!=l && j!=k && j!=l && k!=l) {
							ArrayList<Jugador> subGrupo = new ArrayList<Jugador>();
							subGrupo.add(grupos.get(n).get(i));
							subGrupo.add(grupos.get(n).get(j));
							subGrupo.add(grupos.get(n).get(k));
							subGrupo.add(grupos.get(n).get(l));
							if(controlarCompatibilidad(subGrupo)) {
								ret.add(subGrupo);
							}
						}
					}
				}
			}
		}
		return ret;
	}
	
	public static ArrayList<ArrayList<Jugador>> armarSubGruposDe3(ArrayList<ArrayList<Jugador>> grupos,int n) {
		ArrayList<ArrayList<Jugador>> ret = new ArrayList<ArrayList<Jugador>>();
		for (int i = 0; i < grupos.get(n).size(); i++) {
			for (int j = 0; j < grupos.get(n).size(); j++) {
				for (int k = 0; k < grupos.get(n).size(); k++) {
						if(i!=j && i!=k && j!=k) {
							ArrayList<Jugador> subGrupo = new ArrayList<Jugador>();
							subGrupo.add(grupos.get(n).get(i));
							subGrupo.add(grupos.get(n).get(j));
							subGrupo.add(grupos.get(n).get(k));
							if(controlarCompatibilidad(subGrupo)) {
								ret.add(subGrupo);
							}
						}
					
				}
			}
		}
		return ret;
	}
	
	public static ArrayList<ArrayList<ArrayList<Jugador>>> unirSubGrupos(ArrayList<ArrayList<Jugador>> arqueros,ArrayList<ArrayList<Jugador>> defensores,ArrayList<ArrayList<Jugador>> medios,ArrayList<ArrayList<Jugador>> delanteros){
		ArrayList<ArrayList<ArrayList<Jugador>>> listaCombinatorias=new ArrayList<ArrayList<ArrayList<Jugador>>>();
		listaCombinatorias.add(arqueros);
		listaCombinatorias.add(defensores);
		listaCombinatorias.add(medios);
		listaCombinatorias.add(delanteros);
		return listaCombinatorias;
	}
	
	public static boolean controlarCompatibilidad(Equipo equipo) {
		for (int i = 0; i < equipo.getArquero().size(); i++) {
			if(equipo.getListaNegra().contains(equipo.getArquero().get(i).getNombre())) {
				return false;
			}
		}
		for (int i = 0; i < equipo.getDefensores().size(); i++) {
			if(equipo.getListaNegra().contains(equipo.getDefensores().get(i).getNombre())) {
				return false;
			}
		}
		for (int i = 0; i < equipo.getMediocampistas().size(); i++) {
			if(equipo.getListaNegra().contains(equipo.getMediocampistas().get(i).getNombre())) {
				return false;
			}
		}
		for (int i = 0; i < equipo.getDelanteros().size(); i++) {
			if(equipo.getListaNegra().contains(equipo.getDelanteros().get(i).getNombre())) {
				return false;
			}
		}
		return true;
		
	}
	
	public static boolean controlarCompatibilidad(ArrayList<Jugador> grupo) {
		ArrayList<String> listaNegra=new ArrayList<String>();
		for (int i = 0; i <grupo.size(); i++) {
			listaNegra.add(grupo.get(i).getNoJuegaCon());
		}
		for (int i = 0; i < grupo.size(); i++) {
			if(listaNegra.contains(grupo.get(i).getNombre())) {
				return false;
			}
				
		}
		return true;
	}
	
	public static ArrayList<Equipo> permutarGrupos(ArrayList<ArrayList<ArrayList<Jugador>>> listaCombinatorias){
		ArrayList<Equipo> permutacionesDeEquipos=new ArrayList<Equipo>();
		for (int i = 0; i < listaCombinatorias.get(0).size(); i++) {
			for (int j = 0; j < listaCombinatorias.get(1).size(); j++) {
				for (int k = 0; k < listaCombinatorias.get(2).size(); k++) {
					for (int l = 0; l < listaCombinatorias.get(3).size(); l++) {
						if(i!=j && i!=k && i!=l && j!=k && j!=l && k!=l) {			
							Equipo posibleEquipo=new Equipo(listaCombinatorias.get(0).get(i), listaCombinatorias.get(1).get(j),listaCombinatorias.get(2).get(k),listaCombinatorias.get(3).get(l));
							if(controlarCompatibilidad(posibleEquipo)==true) {
								permutacionesDeEquipos.add(posibleEquipo);
							}
							
						}
					}
				}
			}
		}
		return permutacionesDeEquipos;
	}
	
	public static Equipo devolverMejor(ArrayList<Equipo> candidatos) {
		int indiceDelGanador=0;
		int calidadDeCandidato=0;
		for (int i = 0; i < candidatos.size(); i++) {
			if(candidatos.get(i).getCalidadTotal()>calidadDeCandidato) {
				calidadDeCandidato=candidatos.get(i).getCalidadTotal();
				indiceDelGanador=i;
			}
			
		}
		candidatos.get(indiceDelGanador).setCalidadTotal(calidadDeCandidato);
		return candidatos.get(indiceDelGanador);
	} 

}
