package Datos;

import java.awt.Window.Type;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class InfoJson {
	private ArrayList<Jugador> informacion;
	
	
	
	
	public InfoJson(String nombre){
		Gson gson = new Gson();
		setInformacion(new ArrayList<Jugador>());
		try {
			java.lang.reflect.Type tipoListaJugadores = new TypeToken<ArrayList<Jugador>>(){}.getType();
			BufferedReader br = new BufferedReader(new FileReader(nombre));
			this.informacion=(gson.fromJson(br, tipoListaJugadores));
		} catch (Exception e) {
			
			
		}
	}
	public static String toString(ArrayList<Jugador> j) {
		String ret = "";
		for (int i = 0; i < j.size(); i++) {
			ret+=(j.get(i).getNombre()+" "+j.get(i).getCalidad()+" "+j.get(i).getPuesto()+" "+j.get(i).getNoJuegaCon()+"\n");
		}
		return ret;
	}
	
	public ArrayList<Jugador> getInformacion() {
		return informacion;
	}
	
	public void setInformacion(ArrayList<Jugador> informacion) {
		this.informacion = informacion;
	}
	
}
