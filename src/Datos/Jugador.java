package Datos;

public class Jugador {
	private String nombre;
	private String calidad;
	private String puesto;
	private String noJuegaCon;

	public Jugador(String nombre, String calidad, String puesto, String noJuegaCon) {
		this.nombre = nombre;
		this.calidad = calidad;
		this.puesto = puesto;
		this.noJuegaCon = noJuegaCon;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalidad() {
		return calidad;
	}

	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String posicion) {
		this.puesto = posicion;
	}

	public String getNoJuegaCon() {
		return noJuegaCon;
	}

	public void setNoJuegaCon(String noJuegaCon) {
		this.noJuegaCon = noJuegaCon;
	}

}
