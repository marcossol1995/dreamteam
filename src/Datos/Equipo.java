package Datos;

import java.util.ArrayList;

public class Equipo {
	private ArrayList<Jugador> arquero;
	private ArrayList<Jugador> defensores;
	private ArrayList<Jugador> mediocampistas;
	private ArrayList<Jugador> delanteros;
	public Integer calidadTotal;
	private ArrayList<String> listaNegra;
	
	public Equipo(ArrayList<Jugador> arquero,ArrayList<Jugador> defensores,ArrayList<Jugador> mediocampistas,ArrayList<Jugador> delanteros){
		listaNegra=new ArrayList<String>();
		this.calidadTotal=0;
		this.arquero=arquero;
		this.defensores=defensores;
		this.mediocampistas=mediocampistas;
		this.delanteros=delanteros;
		
		for (int i = 0; i < arquero.size(); i++) {
			this.calidadTotal+=Integer.parseInt(arquero.get(i).getCalidad());
			this.listaNegra.add(arquero.get(i).getNoJuegaCon());
		}
		for (int i = 0; i < defensores.size(); i++) {
			this.calidadTotal+=Integer.parseInt(defensores.get(i).getCalidad());
			this.listaNegra.add(defensores.get(i).getNoJuegaCon());
		}
		for (int i = 0; i < mediocampistas.size(); i++) {
			this.calidadTotal+=Integer.parseInt(mediocampistas.get(i).getCalidad());
			this.listaNegra.add(mediocampistas.get(i).getNoJuegaCon());
		}
		for (int i = 0; i < delanteros.size(); i++) {
			this.calidadTotal+=Integer.parseInt(delanteros.get(i).getCalidad());
			this.listaNegra.add(delanteros.get(i).getNoJuegaCon());
		}
	}
	
	
	public Integer getCalidadTotal() {
		return calidadTotal;
	}


	public void setCalidadTotal(Integer calidadTotal) {
		this.calidadTotal = calidadTotal;
	}


	public ArrayList<String> getListaNegra(){
		return listaNegra;
	}

	public ArrayList<Jugador> getArquero() {
		return arquero;
	}

	public void agregarArquero(Jugador portero) {
		this.arquero.add(portero);
	}

	public ArrayList<Jugador> getDefensores() {
		return defensores;
	}

	public void setDefensores(Jugador defensor) {
		this.defensores.add(defensor);
	}

	public ArrayList<Jugador> getMediocampistas() {
		return mediocampistas;
	}

	public void setMediocampistas(Jugador mediocampista) {
		this.mediocampistas.add(mediocampista);
	}

	public ArrayList<Jugador> getDelanteros() {
		return delanteros;
	}

	public void setDelanteros(Jugador delantero) {
		this.delanteros.add(delantero);
	}
	
	
	
	
}
